﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Media.Imaging;
using TVDBSharp.Models;

namespace Gonix_TV
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : INotifyPropertyChanged
    {

        private ObservableCollection<TvSeries> _fileList = new ObservableCollection<TvSeries>();
        private TvSeries _selectedEntry;

        SelectSeries _selectSeries;
        ConfigWindow _configWindow;



        public ObservableCollection<TvSeries> List
        {
            get { return _fileList; }
            set { _fileList = value; NotifyPropertyChanged("List"); }
        }

        public TvSeries SelectedEntry
        {
            get { return _selectedEntry; }
            set { _selectedEntry = value; NotifyPropertyChanged("SelectedEntry"); }
        }




        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }




        public MainWindow()
        {
            InitializeComponent();

            DataGridView.ItemsSource = List;
        }

        /* STARE DO ZMIANY!!!
        #region DO POPRAWKI!!!!!

        private void DataGridView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataGridView.SelectedItem != null && DataGridView.SelectedItem.GetType().Name == "TvSeries")
            {
                _selectedSeries = (TvSeries) DataGridView.SelectedItem;
            }
        }

        private void AddFilesToDataGrid(string folder)
        {
            _fileList.Clear();
            DataGridView.ItemsSource = null;
            
            if (folder == null)
            {
                return;
            }

            var files = Directory.GetFiles(folder).Where(file => Regex.IsMatch(file, @"^.+\.(mkv|avi|ts|mp4)$")).ToList();
            files.Sort();

            foreach (var tmpSeries in files.Select(f => new TvSeries(f)))
            {
                _fileList.Add(tmpSeries);
            }

            DataGridView.ItemsSource = _fileList;
        }

        private void BTN_ZmienNazwe_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (var episode in _fileList)
                {
                    var oldFile = episode.Path;
                    var extension = episode.Path.Substring(episode.Path.Length - 3);
                    var name = episode.SeriesName;
                    var season = episode.SeasonNumber;
                    var episodenr = episode.EpisodeNumber;
                    var title = episode.EpisodeName;
                    var path = Path.GetDirectoryName(oldFile);
                    var newFile = path + '\\' + CleanFileName(GenerateSeriesName(name, season, episodenr, title, extension));
                    File.Move(oldFile, newFile);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

            AddFilesToDataGrid(_selectSeries.SelectedFolder);
            UpdateListWithNames(_selectSeries.ChoosenShow);
        }

        private static string CleanFileName(string fileName)
        {
            return Path.GetInvalidFileNameChars().Aggregate(fileName, (current, c) => current.Replace(c.ToString(), "_"));
        }

        private static string GenerateSeriesName(string seriesName, string season, string episode, string title, string extension)
        {
            return seriesName + " [" + season + "x" + episode + "] - " + title + "." + extension;
        }
        */

        private void UpdateListWithNames(Show tvShow)
        {
            foreach (var t in _fileList)
            {
                var e = int.Parse(t.EpisodeNumber);
                var s = int.Parse(t.SeasonNumber);
                var episode = tvShow.Episodes.FirstOrDefault(x => x.EpisodeNumber == e && x.SeasonNumber == s);

                if (episode != null)
                {
                    t.EpisodeName = episode.Title;
                }

                t.SeriesName = tvShow.Name;
            }
        }
        
        //#endregion

        
        private void BTN_Settings_OnClick(object sender, RoutedEventArgs e)
        {
            _configWindow = new ConfigWindow {Owner = this};
            _configWindow.ShowDialog();
        }

        private void BTN_SelectSeries_OnClick(object sender, RoutedEventArgs e)
        {
            
            _selectSeries = new SelectSeries { Owner = this };
            if (_selectSeries.ShowDialog() != true)
            {
                return; 
            }

            
            UpdateShowInfo(_selectSeries.ChoosenShow);
            AddFilesToDataGrid(_selectSeries.SelectedFolder);
            UpdateListWithNames(_selectSeries.ChoosenShow);
            
        }

        private void UpdateShowInfo(Show show)
        {
            Poster.Source = new BitmapImage(show.Poster);
            TxtBlockTitle.Text = show.Name;
            LblFirstAired.Content = show.FirstAired != null ? show.FirstAired.Value.ToString("dd.MM.yyyy") : "---";
            LblTvStation.Content = show.Network != string.Empty ? show.Network : "---";
            LblStatus.Content = show.Status;
            LblRating.Content = show.Rating;
            TxtBlockDesc.Text = show.Description;
        }

        private void AddFilesToDataGrid(string folder)
        {
            List.Clear();

            if (string.IsNullOrEmpty(folder))
            {
                return;
            }

            var files = Directory.GetFiles(folder).Where(file => Regex.IsMatch(file, @"^.+\.(mkv|avi|ts|mp4)$")).ToList();
            files.Sort();

            foreach (var tmpSeries in files.Select(f => new TvSeries(f)))
            {
                List.Add(tmpSeries);
            }
        }
    }
}
