﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Gonix_TV.Properties;
using TVDBSharp.Models;
using TVDBSharp;

namespace Gonix_TV
{
    /// <summary>
    /// Interaction logic for SelectSeries.xaml
    /// </summary>
    public partial class SelectSeries
    {
        public string SelectedFolder
        {
            get;
            private set; 
        }

        public Show ChoosenShow
        {
            get;
            private set;
        }

        private List<Show> _results;

        private class NewItem
        {
            public string Name { get; set; }

            public int Files { get; set; }
        }

        public SelectSeries()
        {
            InitializeComponent();
        }

        private void BtnSelectSeries_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateListView(Settings.Default.MainFolder);
        }

        private void UpdateListView(string path)
        {
            var mainFolder = Directory.EnumerateDirectories(path).ToList();
            mainFolder.Sort(); // So the files will be in alfabetical order

            foreach (var singleFolder in mainFolder)
            {
                int files = Directory.EnumerateFiles(singleFolder).Count(file => Regex.IsMatch(file, @"^.+\.(mkv|avi|ts|mp4)$"));
                string name = Path.GetFileName(singleFolder);

                var item = new ListViewItem
                {
                    // Save path to .Tag, so we can easily access it later
                    Tag = singleFolder,
                    Content = new NewItem {Name = name, Files = files}
                };
                LvFolders.Items.Add(item);
            }
        }

        private void LvFolders_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LvFolders.SelectedItem == null)
            {
                return;
            }

            var choosen = (ListViewItem)LvFolders.SelectedItem;
            
            // Save full path tu SelectedFolder
            SelectedFolder = choosen.Tag.ToString();

            // BackGroundWorker to search thru TVDB.com database
            var worker = new BackgroundWorker();
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;

            worker.RunWorkerAsync();

            // Clear results in TVDB search
            Poster.Source = new BitmapImage(new Uri("Resources/no-poster.png", UriKind.Relative));
            SearchResults.ItemsSource = null;

            LblTvdbSearch.Content = "Wyszukiwanie...";
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SearchResults.ItemsSource = _results;
            LblTvdbSearch.Content = "Wyszukiwanie zakończone!";
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var tvdb = new TVDB("8DE2C125BD9D143E");
            _results = tvdb.Search(Path.GetFileName(SelectedFolder), 15);
        }

        private void TvdbSearchResults_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // If nothing selected show 'no-poster.png'
            if (SearchResults.SelectedItem == null)
            {
                Poster.Source = new BitmapImage(new Uri("Resources/no-poster.png", UriKind.Relative));
                BtnSelectSeries.IsEnabled = false;
                ChoosenShow = null;
                TxtBlockDescription.Text = string.Empty;
                return;
            }

            BtnSelectSeries.IsEnabled = true;

            // Show chosen in TVDB search
            ChoosenShow = (Show) SearchResults.SelectedItem;

            TxtBlockDescription.Text = ChoosenShow.Description;

            // If there is no poster available, show 'no-poster.png'
            if (ChoosenShow.Poster == new Uri("http://thetvdb.com/banners/"))
            {
                Poster.Source = new BitmapImage(new Uri("Resources/no-poster.png", UriKind.Relative));
            }
            else
            {
                Poster.Source = new BitmapImage(ChoosenShow.Poster);
            }
            
        }

        private void TextBlock_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (ChoosenShow != null)
            {
                System.Diagnostics.Process.Start(string.Format("http://www.imdb.com/title/{0}/", ChoosenShow.ImdbId));
            }
        }
    }
}
