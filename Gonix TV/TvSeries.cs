﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Gonix_TV
{
    public class TvSeries
    {
        private string _seasonNumber;
        private string _episodeNumber;

        public TvSeries(string path)
        {
            Path = path;
            SeriesName = SerName(path);
            SeasonNumber = EpSeasonNumber(path);
            EpisodeNumber = EpNumber(path);
            EpisodeName = EpName(path);
            FileName = FName(path);
            HaveSubtitles = HaveSubs(path);
        }


        public TvSeries()
        {
        }

        public string Path { get; private set; }

        public string SeriesName { get; set; }

        public string SeasonNumber
        {
            get { return _seasonNumber; }

            set
            {
                switch (value.Length)
                {
                    case 1:
                        _seasonNumber = "0" + value;
                        break;
                    case 2:
                        _seasonNumber = value;
                        break;
                    default:
                        _seasonNumber = string.Empty;
                        break;
                }
            }
        }

        public string EpisodeNumber
        {
            get { return _episodeNumber; }

            set
            {
                switch (value.Length)
                {
                    case 1:
                        _episodeNumber = "0" + value;
                        break;
                    case 2:
                        _episodeNumber = value;
                        break;
                    default:
                        _episodeNumber = string.Empty;
                        break;
                }
            }
        }

        public string EpisodeName { get; set; }

        public string FileName { get; set; }

        public bool HaveSubtitles { get; set; }

        private static string EpName(string path)
        {
            path = System.IO.Path.GetFileNameWithoutExtension(path);
            const string czegoSzukac = "] - ";
            var pozycja = path.IndexOf(czegoSzukac, StringComparison.Ordinal);
            if (pozycja != -1)
            {
                return path.Substring(pozycja + czegoSzukac.Length);
            }
            return string.Empty;
        }

        private static string SerName(string path)
        {
            var nameRegex = new Regex(@"^(?<name>(.)+) \[\d{1,2}|^(?<name>(.)+) [Ss]\d{1,2}");
            var tmp = Regex.Replace(System.IO.Path.GetFileNameWithoutExtension(path), @"(?<=[a-z])\.", " ");
            var nameMatch = nameRegex.Match(tmp);

            if (nameMatch.Success)
            {
                return nameMatch.Groups["name"].ToString();
            }

            return Regex.Replace(System.IO.Path.GetFileNameWithoutExtension(path), @"(?<=[a-z])\.", " ");
        }

        private static string EpNumber(string path)
        {
            var episodeRegex = new Regex(@"\d{1,2}[Xx](?<episode>(\d{1,2})+)|[Ss]\d{1,2}[Ee](?<episode>(\d{1,2})+)");
            var episodeMatch = episodeRegex.Match(path);
            return episodeMatch.Groups["episode"].ToString();
        }

        private static string EpSeasonNumber(string path)
        {
            var seasonRegex = new Regex(@"(?<season>(\d{1,2})+)x\d{1,2}|(?<season>(\d{1,2})+)[Ee]\d{1,2}");
            var seasonMatch = seasonRegex.Match(path);
            return seasonMatch.Groups["season"].ToString();
        }

        private static string FName(string path)
        {
            return System.IO.Path.GetFileName(path);
        }


        private static bool HaveSubs(string path)
        {
            return File.Exists(path.Remove(path.Length - 3) + "srt");
        }
    }
}
