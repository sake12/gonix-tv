﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Gonix_TV.Properties;

namespace Gonix_TV
{
    /// <summary>
    /// Interaction logic for ConfigWindow.xaml
    /// </summary>
    public partial class ConfigWindow
    {
        private bool _n24LoginCheck;
        private BackgroundWorker loginWorker = new BackgroundWorker();
        private string _login, _password;

        public ConfigWindow()
        {
            InitializeComponent();

            loginWorker.DoWork += loginWorker_DoWork;
            loginWorker.RunWorkerCompleted += loginWorker_RunWorkerCompleted;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Shows saved settings in window's controls
            UpdateData();
        }

        private void BtnSave_OnClick(object sender, RoutedEventArgs e)
        {
            // Save settings and close window
            DialogResult = true;
            SaveData();
            Close();
        }

        private void N24Login_TextChanged(object sender, RoutedEventArgs e)
        {
            if (N24LoginBox.Text.Length > 3 && N24PasswordBox.Password.Length > 3 && N24PasswordBox.Password.Length < 16)
            {
                LblN24Login.Content = "Kliknij 'Zaloguj się...'";
                BtnLogIn.IsEnabled = true;
            }
            else
            {
                LblN24Login.Content = "Wprowadź poprawne dane...";
                BtnLogIn.IsEnabled = false;
            }
        }

        private void BtnLogIn_OnClick(object sender, RoutedEventArgs e)
        {
            LblN24Login.Content = "Logowanie na stronie...";
            
            if (loginWorker.IsBusy != true)
            {
                N24LoginBox.IsEnabled = false;
                N24PasswordBox.IsEnabled = false;

                _login = N24LoginBox.Text;
                _password = N24PasswordBox.Password;

                loginWorker.RunWorkerAsync();
            }
        }

        private void UpdateData()
        {
            N24LoginBox.Text = Settings.Default.N24Login;
            N24PasswordBox.Password = Settings.Default.N24Password;
            TxtDownloadFolder.Text = Settings.Default.DownloadFolder;
            TxtMainFolder.Text = Settings.Default.MainFolder;
        }

        private void SaveData()
        {
            // If user verified credentials we can save it, else skip save
            if (_n24LoginCheck)
            {
                Settings.Default.N24Login = N24LoginBox.Text;
                Settings.Default.N24Password = N24PasswordBox.Password;
            }

            // GroupBox 'Foldery'
            // Save directory only if it exists
            if (Directory.Exists(TxtDownloadFolder.Text))
            {
                Settings.Default.DownloadFolder = TxtDownloadFolder.Text;
            }
            if (Directory.Exists(TxtMainFolder.Text))
            {
                Settings.Default.MainFolder = TxtMainFolder.Text;
            }

            // With out this settings won't be saved
            Settings.Default.Save();
        }

        private void loginWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Here we test if we can login to napisy24.pl site
            _n24LoginCheck = TestLoginData(_login, _password);
        }

        private void loginWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (_n24LoginCheck)
            {
                LblN24Login.Content = "Dane prawidłowe!\nKliknij 'Zapisz'...";
                BtnLogIn.IsEnabled = false;
            }
            else
            {
                LblN24Login.Content = "Wpisałeś złe dane!";
                BtnLogIn.IsEnabled = true;

                N24LoginBox.IsEnabled = true;
                N24PasswordBox.IsEnabled = true;
            }
        }

        private bool TestLoginData(string user, string pass)
        {
            //TODO Napisać logikę!!!!

            Thread.Sleep(1000);

            return false;
        }

        private void BtnMainFolder_OnClick(object sender, RoutedEventArgs e)
        {
            var folderDialog = new FolderBrowserDialog();

            DialogResult result = folderDialog.ShowDialog();
            if (result.ToString() == "OK")
            {
                TxtMainFolder.Text = folderDialog.SelectedPath;
            }
        }

        private void BtnDownloadFolder_OnClick(object sender, RoutedEventArgs e)
        {
            var folderDialog = new FolderBrowserDialog();

            DialogResult result = folderDialog.ShowDialog();
            if (result.ToString() == "OK")
            {
                TxtDownloadFolder.Text = folderDialog.SelectedPath;
            }
        }
    }
}
